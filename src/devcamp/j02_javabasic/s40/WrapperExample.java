package devcamp.j02_javabasic.s40;

public class WrapperExample {
    public static void autoboxing(){
        byte bte = 10;
        short sh = 20;
        int it = 30;
        long lng = 40;
        float fat = 50.1f;
        double dbl = 60.0d;
        char ch = 'a';
        boolean boo = true;
        /*** Autoboxing: Converting primitives into object
     * Autoboxing là cơ chế tự động chuyển đổi kiểu dữ liệu nguyên thủy sang objec
     *  của wrapper class tương ứng.
     */
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long lngobj = lng;
        Float floatobj = fat;
        Double dbobj = dbl;
        Character charobj = ch;
        Boolean boolobj = boo;

        System.out.println("--Printing object values (In gia tri cua object--");
        System.out.println("Byte object: " + byteobj);
        System.out.println("Short object: " + shortobj);
        System.out.println("Integer object: " + intobj);
        System.out.println("Long object: " + lngobj);
        System.out.println("Float object: " + floatobj);
        System.out.println("Double object: " + dbobj);
        System.out.println("Character object: " + boolobj);
        System.out.println("Boolean object: " + charobj);
    }
   
    public static void unboxing(){
        byte bte = 11;
        short sh = 22;
        int it = 33;
        long lng = 44;
        float fat = 55.5f;
        double dbl = 66.6d;
        char ch = 'b';
        boolean boo = false;
        //Autoboxing: Converting primitives into object
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long lngobj = lng;
        Float floatobj = fat;
        Double dbobj = dbl;
        Character charobj = ch;
        Boolean boolobj = boo;
         /***Unboxing: Converting object primitives to primitives
         * Unboxing là cơ chees giúp chuyển đổi các object của wrapper class sang kiểu
         * dữ liệu tương ứng
         */
        byte btevalue = byteobj;
        short shortvalue = shortobj;
        int intvalue = intobj;
        long lngvalue = lngobj;
        float floatvalue = floatobj;
        double dbvalue = dbobj;
        char charvalue = charobj;
        boolean boolvalue = boolobj;

        System.out.println("--Printing object values (In gia tri cua object--");
        System.out.println("Byte value: " + btevalue);
        System.out.println("Short value: " + shortvalue);
        System.out.println("Integer value: " + intvalue);
        System.out.println("Long value: " + lngvalue);
        System.out.println("Float value: " + floatvalue);
        System.out.println("Double value: " + dbvalue);
        System.out.println("Character value: " + charvalue);
        System.out.println("Boolean value: " + boolvalue);
    }
    public static void main(String[] args) throws Exception {
        WrapperExample.autoboxing();

        WrapperExample.unboxing();
    }
}
